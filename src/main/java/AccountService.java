import java.math.BigDecimal;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.math.BigDecimal.ZERO;

public class AccountService {

  private static final AccountService INSTANCE = new AccountService();

  private ConcurrentHashMap<String, Account> accountMap = new ConcurrentHashMap<>();

  private AccountService() {
  }

  public static AccountService getService() {
    return INSTANCE;
  }

  public Optional<Account> findByNumber(String number) {
    return Optional.ofNullable(accountMap.getOrDefault(number, null));
  }

  public void addAccount(Account account) {
    accountMap.put(account.getNumber(), account);
  }

  public void deleteAccount(String account) {
    accountMap.remove(account);
  }

  @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
  public Result transfer(Account fromAccount, Account toAccount, BigDecimal amount) {

    if (amount.compareTo(ZERO) <= 0) {
      return Result.fail("Amount should be a positive number");
    }

    final Account lockFirst;
    final Account lockSecond;
    if (fromAccount.getNumber().compareTo(toAccount.getNumber()) > 0) {
      lockFirst = fromAccount;
      lockSecond = toAccount;
    } else {
      lockFirst = toAccount;
      lockSecond = fromAccount;
    }

    synchronized (lockFirst) {
      synchronized (lockSecond) {

        if (fromAccount.getAmount().subtract(amount).compareTo(ZERO) < 0) {
          return Result.fail("Not enough money on account");
        }

        fromAccount.setAmount(fromAccount.getAmount().subtract(amount));
        toAccount.setAmount(toAccount.getAmount().add(amount));
      }
    }

    return new Result(true, null);
  }
}
