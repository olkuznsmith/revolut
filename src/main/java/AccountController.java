import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.Optional;

@Path("account")
public class AccountController {

  @GET
  @Path("/transfer")
  @Produces(MediaType.APPLICATION_JSON)
  public Result transfer(
      @QueryParam("fromAccount") String fromAccount,
      @QueryParam("toAccount") String toAccount,
      @QueryParam("amount") BigDecimal amount
  ) {
    Optional<Account> accountFrom = AccountService.getService().findByNumber(fromAccount);
    if (!accountFrom.isPresent()) {
      return Result.fail("Account=" + fromAccount + " does not exist");
    }
    Optional<Account> accountTo = AccountService.getService().findByNumber(toAccount);
    if (!accountTo.isPresent()) {
      return Result.fail("Account=" + toAccount + " does not exist");
    }
    if (accountFrom.equals(accountTo)) {
      return Result.fail("Cannot transfer to the same account");
    }

    return AccountService.getService().transfer(accountFrom.get(), accountTo.get(), amount);
  }
}
