import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.ext.RuntimeDelegate;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;

public class App {

  static HttpServer startServer() throws IOException {
    HttpServer server = HttpServer.create(new InetSocketAddress(getBaseURI().getPort()), 0);
    Runtime.getRuntime().addShutdownHook(new Thread(() -> server.stop(0)));

    HttpHandler handler = RuntimeDelegate.getInstance().createEndpoint(new JaxRsApplication(), HttpHandler.class);

    server.createContext(getBaseURI().getPath(), handler);

    server.start();

    return server;
  }

  public static void main(String[] args) throws IOException, InterruptedException {

    startServer();

    Thread.currentThread().join();
  }

  private static int getPort(int defaultPort) {
    String port = System.getProperty("jersey.config.test.container.port");
    if (port != null) {
      return Integer.parseInt(port);
    }
    return defaultPort;
  }

  public static URI getBaseURI() {
    return UriBuilder.fromUri("http://localhost/").port(getPort(8080)).build();
  }
}
