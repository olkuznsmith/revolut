public class Result {

  private boolean success;

  private String error;

  public Result() {
  }

  public Result(boolean success, String error) {
    this.success = success;
    this.error = error;
  }

  public static Result fail(String error) {
    return new Result(false, error);
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }
}
