import com.sun.net.httpserver.HttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AccountControllerTest {

  private static final String ACCOUNT_ONE = "1";
  private static final String ACCOUNT_TWO = "2";

  private HttpServer server;

  @Before
  public void setUp() throws IOException {
    AccountService.getService().deleteAccount(ACCOUNT_ONE);
    AccountService.getService().deleteAccount(ACCOUNT_TWO);

    AccountService.getService().addAccount(new Account(ACCOUNT_ONE, new BigDecimal(100)));
    AccountService.getService().addAccount(new Account(ACCOUNT_TWO, new BigDecimal(100)));

    this.server = App.startServer();
  }

  @After
  public void tearDown() {
    server.stop(0);
  }

  @Test
  public void testTransfer_succeeded() throws Exception {

    WebTarget target = ClientBuilder.newClient().target(App.getBaseURI() + "account/transfer")
        .queryParam("fromAccount", ACCOUNT_ONE)
        .queryParam("toAccount", ACCOUNT_TWO)
        .queryParam("amount", "10");

    Result result = target.request(MediaType.APPLICATION_JSON_TYPE).get().readEntity(Result.class);

    assertTrue(result.isSuccess());
    assertAmountEquals(ACCOUNT_ONE, 90);
    assertAmountEquals(ACCOUNT_TWO, 110);
  }

  private void assertAmountEquals(String accountNumber, int amount) {
    AccountService service = AccountService.getService();
    assertTrue(service.findByNumber(accountNumber).get().getAmount().compareTo(new BigDecimal(amount)) == 0);
  }

  @Test
  public void testTransfer_notEnoughMoney() throws Exception {

    WebTarget target = ClientBuilder.newClient().target(App.getBaseURI() + "account/transfer")
        .queryParam("fromAccount", ACCOUNT_ONE)
        .queryParam("toAccount", ACCOUNT_TWO)
        .queryParam("amount", "1000");

    Result result = target.request(MediaType.APPLICATION_JSON_TYPE).get().readEntity(Result.class);

    assertFalse(result.isSuccess());
    assertEquals("Not enough money on account", result.getError());

    assertAmountEquals(ACCOUNT_ONE, 100);
    assertAmountEquals(ACCOUNT_TWO, 100);
  }

  @Test
  public void testTransfer_negativeAmount() throws Exception {

    WebTarget target = ClientBuilder.newClient().target(App.getBaseURI() + "account/transfer")
        .queryParam("fromAccount", ACCOUNT_ONE)
        .queryParam("toAccount", ACCOUNT_TWO)
        .queryParam("amount", "-100");

    Result result = target.request(MediaType.APPLICATION_JSON_TYPE).get().readEntity(Result.class);

    assertFalse(result.isSuccess());
    assertEquals("Amount should be a positive number", result.getError());

    assertAmountEquals(ACCOUNT_ONE, 100);
    assertAmountEquals(ACCOUNT_TWO, 100);
  }

  @Test
  public void testTransfer_transferToSameAccount() throws Exception {

    WebTarget target = ClientBuilder.newClient().target(App.getBaseURI() + "account/transfer")
        .queryParam("fromAccount", ACCOUNT_ONE)
        .queryParam("toAccount", ACCOUNT_ONE)
        .queryParam("amount", "10");

    Result result = target.request(MediaType.APPLICATION_JSON_TYPE).get().readEntity(Result.class);

    assertFalse(result.isSuccess());
    assertEquals("Cannot transfer to the same account", result.getError());
  }

  @Test
  public void testTransfer_accountDoesNotExist() throws Exception {

    AccountService.getService().deleteAccount(ACCOUNT_TWO);

    WebTarget target = ClientBuilder.newClient().target(App.getBaseURI() + "account/transfer")
        .queryParam("fromAccount", ACCOUNT_ONE)
        .queryParam("toAccount", ACCOUNT_TWO)
        .queryParam("amount", "10");

    Result result = target.request(MediaType.APPLICATION_JSON_TYPE).get().readEntity(Result.class);

    assertFalse(result.isSuccess());
    assertEquals("Account=" + ACCOUNT_TWO + " does not exist", result.getError());
  }
}